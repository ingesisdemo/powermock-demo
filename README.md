**Powermock demo** <br>
Do bytecode manipulation to mock complex scenarios like static methods, suppress methods and intercept calls to new class.
Useful to test code that have not been designed for testability.

A good use case for PowerMock is to write test before refactoring a complex legacy code.

The EvilConstructor show how to suppress code with PowerMock.

To run tests with PowerMock, you need to use the PowerMockRunner and define classes where bytecode will be modified.

Since a static initializer cause an error when a library is missing, the test need to suppress the static initialization.
To do that, add the annotation to the test class.

```
@RunWith(PowerMockRunner.class)
@PrepareForTest({ EvilConstruct.class })
@SuppressStaticInitializationFor("com.ingesis.demo.evil.EvilConstruct")
public class EvilConstructTest
```


The constructor of the abstract parent also fail.  You can suppress it with the following line.

```
@BeforeClass
public static void init() {
	PowerMockito.suppress(PowerMockito.constructor(AbstractEvilConstruct.class));
}
```


After that, you can create your class to test without problem.

```
@Before
public void setup() {
	classToTest = new EvilConstruct();
}
```


Since the method to test calls a private method that cause problem, it is possible to suppress the method.

```
PowerMockito.suppress(PowerMockito.method(EvilConstruct.class, "loadLib"));
```


When you suppress a constructor or a method, you can suppress desirable code.
In ItemService, the encapsulated list is created with the constructor.  Without the constructor, the list is never initialized.
Since the list is encapsulated, you cannot create a new list and call the setter.
To overcome the problem, you can use Whitebox.

An example of the whitebox can be find in ItemServiceTest.

```
@Test
public void testSupression() {
	PowerMockito.suppress(PowerMockito.constructor(ItemService.class));
	ItemService service = new ItemService();
	// Suppressing the constructor also suppress the list creation
	Whitebox.setInternalState(service, ConcurrentLinkedQueue.class, new ConcurrentLinkedQueue<Item>());
}	
```

DiceTest show how to intercept a call to new.  
If you have checked the mockito-demo, the class Dice create a new Random inside the method instead of using a field.
Mockito cannot be used in that scenario.
You can do it with PowerMock by using whenNew.  After that, every call to new Random will return your mock.

```
@Before
public void setup() throws Exception {
	rand = Mockito.mock(Random.class);
	PowerMockito.whenNew(Random.class).withNoArguments().thenReturn(rand);
	dice = new Dice();
}
```

DiceTest also show an example how to use 2 frameworks together with JunitRule
You can use only 1 runner.  To use JUnitParams with PowerMock, you need to define one of them as a JunitRule.

```
@RunWith(JUnitParamsRunner.class)
public class DiceTest {

	@Rule
	public PowerMockRule rule = new PowerMockRule();

}
```




ListTests contains a complex scenario.

```
@BeforeClass
public static void init() throws Exception {

	LocalDate now = LocalDate.now();
	LocalDate other = now.minusYears(4500).minusMonths(3).plusDays(21);		
	
	PowerMockito.mockStatic(LocalDate.class);
	PowerMockito.when(LocalDate.now()).thenReturn(now).thenReturn(other);
	
}
```

In the previous example, PowerMock replace all static calls to LocalDate and allows to define behaviours like with Mockito.

You need to be careful when you do this.  The default behaviour of a mock is to return null if the original method return an Object.
Moving mockStatic at the top will throw a NullPointerException.  The reason is that the LocalDate.now is called after the mock so, it returns null.

```
@BeforeClass
public static void init() throws Exception {

	PowerMockito.mockStatic(LocalDate.class);

	LocalDate now = LocalDate.now();
	LocalDate other = now.minusYears(4500).minusMonths(3).plusDays(21);		
	
	PowerMockito.when(LocalDate.now()).thenReturn(now).thenReturn(other);
	
}
```

The class Integer is final and cannot be mocked. 
You overcome some of the limitations by creating a spy instead of a mock.

```
@BeforeClass
public static void init() throws Exception {

    PowerMockito.spy(Integer.class);
    PowerMockito.when(Integer.valueOf(ArgumentMatchers.anyInt())).thenReturn(1).thenReturn(2);

}
```

A spy do not overwrite all methods, it keeps the real methods and allow to change the behaviour of a method.
You also need to be careful when using a spy.  
If you change the behaviour of a method and call a real method, it is possible that the real method calls the method you modified.


Documentation https://github.com/powermock/powermock/wiki