package com.ingesis.demo.evil;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EvilConstruct.class })
@SuppressStaticInitializationFor("com.ingesis.demo.evil.EvilConstruct")
public class EvilConstructTest {

	private EvilConstruct classToTest;
	
	@BeforeClass
	public static void init() {
		PowerMockito.suppress(PowerMockito.constructor(AbstractEvilConstruct.class));
	}
	
	@Before
	public void setup() {
		classToTest = new EvilConstruct();
	}
	
	@Test
	public void testSize() {
		PowerMockito.suppress(PowerMockito.method(EvilConstruct.class, "loadLib"));
		int result = classToTest.size();
		Assertions.assertThat(result).isEqualTo(0);
	}
}
