package com.ingesis.demo;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.rule.PowerMockRule;

import com.ingesis.demo.Dice;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
@PrepareForTest({ Dice.class })
public class DiceTest {

	@Rule
	public PowerMockRule rule = new PowerMockRule();

	
	private Random rand;
	private Dice dice;
	
	@Before
	public void setup() throws Exception {
		rand = Mockito.mock(Random.class);
		PowerMockito.whenNew(Random.class).withNoArguments().thenReturn(rand);
		dice = new Dice();
	}
	
	
	@Test
	@Parameters({ "3, 1, 6", "0, 0, 2", "5, 5, 12", "0, 5, 7" }) 
	public void throwDices_2(int p1, int p2, int expected) {
		//Chain return results, the first call to the mock will return p1 and the second return p2. 
		when(rand.nextInt(anyInt())).thenReturn(p1).thenReturn(p2);
		int result = dice.throwDices(2, 6);
		Assertions.assertThat(result).isEqualTo(expected);
	}
	
	@Test(expected = ClassCastException.class)
	public void throwDices_exception() {
		when(rand.nextInt(anyInt())).thenReturn(1).thenReturn(2).thenThrow(new ClassCastException("Result of mock"));
		dice.throwDices(3, 6);
		Assertions.fail("Should have thrown an exception");
	}

	
}
