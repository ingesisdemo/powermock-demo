package com.ingesis.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.ingesis.demo.MockingDemoService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ MockingDemoService.class, List.class, ArrayList.class, Integer.class, LocalDate.class })
public class ListTests {
	
	
	@BeforeClass
	public static void init() throws Exception {
		ArrayList list = Mockito.mock(ArrayList.class);
		Mockito.when(list.size()).thenReturn(0, 1, 5, -1);
		Mockito.doThrow(ClassCastException.class).when(list).clear();;
		PowerMockito.whenNew(ArrayList.class).withNoArguments().thenReturn(list);
		
		LocalDate now = LocalDate.now();
		LocalDate other = now.minusYears(4500).minusMonths(3).plusDays(21);		
		
		PowerMockito.mockStatic(LocalDate.class);
		PowerMockito.when(LocalDate.now()).thenReturn(now).thenReturn(other);
		
		PowerMockito.spy(Integer.class);
		PowerMockito.when(Integer.valueOf(ArgumentMatchers.anyInt())).thenReturn(1).thenReturn(2);
	}
	
	@Test
	public void add() {
		MockingDemoService service = new MockingDemoService();
		boolean result = service.execute();
		Assertions.assertThat(result).isTrue();
		
	}

}
