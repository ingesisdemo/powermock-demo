package com.ingesis.demo;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.ingesis.demo.Item;
import com.ingesis.demo.ItemFactory;
import com.ingesis.demo.ItemService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ItemService.class })
public class ItemServiceTest {

	
	@Test
	public void testSupression() {
		PowerMockito.suppress(PowerMockito.constructor(ItemService.class));
		ItemService service = new ItemService();
		Whitebox.setInternalState(service, ConcurrentLinkedQueue.class, new ConcurrentLinkedQueue<Item>());
		Stream<Item> result = service.list(100);
		Assertions.assertThat(result).isEmpty();
		
		service.save(ItemFactory.create());
		result = service.list(100);
		Assertions.assertThat(result).hasSize(1);
		
	}
	
}
