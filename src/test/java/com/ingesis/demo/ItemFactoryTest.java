package com.ingesis.demo;

import java.time.LocalDate;
import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.ingesis.demo.Item;
import com.ingesis.demo.ItemFactory;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ LocalDate.class, ItemFactory.class })
public class ItemFactoryTest {
	
	
	
	@BeforeClass
	public static void init() {
	//	PowerMockito.mockStatic(LocalDate.class);
	}
	
	@Test
	public void testCreate() {
		LocalDate date = LocalDate.of(2019, 6, 22);
		PowerMockito.mockStatic(LocalDate.class);
		
		
		Random rand = Mockito.mock(Random.class);
		Mockito.when(rand.nextInt(ArgumentMatchers.anyInt())).thenReturn(500);
		Whitebox.setInternalState(ItemFactory.class, Random.class, rand);
		
		PowerMockito.when(LocalDate.now()).thenReturn(date);
		
		Item item = ItemFactory.create();
		Assertions.assertThat(item.getDate()).isEqualTo(date);
		Assertions.assertThat(item.getDate()).isNotNull();
		

	}

	
}
