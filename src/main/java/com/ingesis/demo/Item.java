package com.ingesis.demo;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Item {

	private int id;
	private LocalDate date;
	private double value;
}
