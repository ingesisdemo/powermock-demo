package com.ingesis.demo;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.extern.slf4j.Slf4j;

public class ItemFactory {

	private static Random rand = new Random();
	private static AtomicInteger idGen = new AtomicInteger();
	
	
	public static Item create() {
		int dayDiff = rand.nextInt(1000) - 500; 
		System.out.println("Diff: " + dayDiff);
		LocalDate now = LocalDate.now();
		System.out.println("Now: " + now);
		LocalDate date = now.minusDays(dayDiff);
		return new Item(idGen.incrementAndGet(), date, 1000 * rand.nextDouble());
		
	}
	
}
