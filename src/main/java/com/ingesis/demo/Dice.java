package com.ingesis.demo;

import java.util.Random;

public class Dice {
	
	
	public int throwDices(int quantity, int numFaces) {
		Random rand = new Random();
		int total = 0;
		for(int i = 0; i < quantity; i++) {
			total += 1 + rand.nextInt(numFaces);
		}
		return total;
	}

}
