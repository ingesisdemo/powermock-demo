package com.ingesis.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import io.reactivex.Flowable;

public class ItemService {

	private ConcurrentLinkedQueue<Item> list = new ConcurrentLinkedQueue<>();
	
	public ItemService() {
		Random rand = new Random();
		int size = 10 + rand.nextInt(1000);
		int interval = 200 + rand.nextInt(1000);
		Flowable.interval(interval, TimeUnit.MILLISECONDS)
			.take(size)
			.forEach(x -> {
				list.add(ItemFactory.create());
				
			});
		
	}
	
	public Stream<Item> list(int limit)  {
		if(System.currentTimeMillis() % 1000  == 2) {
			throw new RuntimeException("You are unlucky");
		}
		List<Item> current = new ArrayList<Item>(list);
		Collections.shuffle(current);
		return current.stream().limit(limit);
	}
	
	public void save(Item item) {
		list.add(item);
	}
}
