package com.ingesis.demo;

import java.time.LocalDate;
import java.util.ArrayList;

public class MockingDemoService {

	public boolean execute() {
		try {
			ArrayList<String> list = new ArrayList<>();
			System.out.println(list.size());
			list.add("A");
			System.out.println(list.size());
			list.add("B");
			System.out.println(list.size());
			
			System.out.println(list.size());
			list.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("**********************");
		int addResult = addIntegers();
		System.out.println("Result: " + addResult);
		
		System.out.println("**********************");
		showDates();
		
		return true;
	}
	
	private int addIntegers() {
		Integer value1 = Integer.valueOf(1);
		System.out.println(value1);
		
		Integer value2 = Integer.valueOf(5);
		System.out.println(value2);
		return value1 + value2;
		
	}
	
	private void showDates() {
		LocalDate date = LocalDate.now();
		System.out.println(date);
		
		LocalDate date2 = LocalDate.now();
		System.out.println(date2);
		
	}
}
