package com.ingesis.demo.evil;

import java.util.ArrayList;
import java.util.List;

public class EvilConstruct extends AbstractEvilConstruct {
	
	private List<String> list = new ArrayList<String>();
	
	static {
		System.loadLibrary("evil.dll");
	}

	public EvilConstruct() {
		super();
	}
	
	public int size() {
		loadLib();
		return list.size();
	}
	
	private void loadLib() {
		System.loadLibrary("evil.dll");

	}
}
